$(document).ready(function(){




		/* ------------------------------------------------
		ANCHOR START
		------------------------------------------------ */
				 
			    function goUp(){
					var windowHeight = $(window).height(),
						windowScroll = $(window).scrollTop();

					if(windowScroll>windowHeight/2){
						$('.arrow_up').addClass('active');
					}

					else{
						$('.arrow_up').removeClass('active');
					}

			    }

			    goUp();
				$(window).on('scroll',goUp);

				$('.arrow_up').on('click ontouchstart',function () {

					if($.browser.safari){
						$('body').animate( { scrollTop: 0 }, 1100 );
					}
					else{
						$('html,body').animate( { scrollTop: 0}, 1100 );
					}
					return false;
					
				});

		/* ------------------------------------------------
		ANCHOR END
		------------------------------------------------ */



		/* ------------------------------------------------
		ACCORDION START
		------------------------------------------------ */

				if($('.accordion').length){
					$('[data-target-accord-title]').on('click', function(e){

						var current = $(this),
							target  = current.data('target-accord-title'), 
						 	position = $('[data-target-accordion="'+target+'"]').offset().top;

						 if(current.hasClass('active')){
						 	$('[data-target-accordion="'+target+'"]').slideUp();
						 	current.removeClass("active");
						 }
						 else{
						 	current.addClass("active");
							$('[data-target-accordion="'+target+'"]').slideDown(500);
						 	
						 	setTimeout(function(){
						 		$('html, body').stop().animate({
							        scrollTop: $('[data-target-accordion="'+target+'"]').offset().top
							    }, 1200,'easeOutCubic', function(){
							    	highlite_menu();
							    });
						 	}, 500);

						 }

						e.preventDefault();
					});  
				}

					$('[data-editable]').on("click", function(event) {
						event.preventDefault();
					})


		/* ------------------------------------------------
		ACCORDION END
		------------------------------------------------ */



		/* ------------------------------------------------
		RESPONSIVE MENU START
		------------------------------------------------ */

				$(".resp_btn, .close_resp_menu").on("click ontouchstart", function(){
					$("body").toggleClass("show_menu")
				});

				$(document).on("click ontouchstart", function(event) {
			      if ($(event.target).closest("nav,.resp_btn").length) return;
			      $("body").removeClass("show_menu");
			      if(windowW <= 991){
			      	$(".menu_item").removeClass("active").find(".dropdown-menu").css("display","none");
			      }
			      event.stopPropagation();
			    });

				
				// проверка на наличие элемента и вставка хтмл кода


				$('.menu_link').on('click ontouchstart',function(event){
					if($("html").hasClass("md_no-touch"))return;

			        var windowWidth = $(window).width(),
			            $parent = $(this).parent('.menu_item');
			        if(windowWidth > 991){
			          // if($("html").hasClass("md_touch")){
			            if((!$parent.hasClass('active')) && $parent.find('.dropdown-menu').length){

			              event.preventDefault();

			              $parent.toggleClass('active')
			               .siblings()
			               .find('.menu_link')
			               .removeClass('active');
			            }
			          // }  
			        }
			        
			        else{
			            
			          if((!$parent.hasClass('active')) && $parent.find('.dropdown-menu').length){

			            event.preventDefault();

			            $parent.toggleClass('active')
			             .siblings()
			             .removeClass('active');
			            $parent.find(".dropdown-menu")
			             .slideToggle()
			             .parents('.menu_item')
			             .siblings()
			             .find(".dropdown-menu")
			             .slideUp();
			          }
			        }

			    });





		
});
