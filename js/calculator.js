$(document).ready(function(){

/*------------------------ RPS ---------------------------*/

				var budgetSlider = $('#budgetSlider'),
					budgetSliderOutput = $('#budgetAmount'),
					budgetMarkers = $('.budget_markers'),
					RPSSlider = $('#rpsSlider'),
					RPSSliderOutput = $('#rpsAmount'),
					RPSMarkers = $('.rps_markers');

				/*------------------------ Sliders settings ---------------------------*/

					var mode = 1, // mode (1 = from budget to RPS, 2 = frfom RPS to budget)
						units = 1000000, // units for RPS slider (millions)

					// Default values for budget Slider
					budgetSliderSettings = {
						min: 0,
						max: 40000,
						value: 38500,
						step: 2000
					},

					// Default values fro RPS Slider
					RPSSliderSettings = {
						min: 0,
						max: 10000000,
						step: 500000
					}

					// convert function (from budget to RPS)
					function fromBudgetToRPS(budget){
						return budget * 250;
					}

					// convert function (from RPS to budget)
					function fromRPStoBudget(RPS){
						return RPS / 250;
					}

				/*------------------------ End of Sliders settings ---------------------------*/

				/*------------------------ Logic ---------------------------*/

					function calculateValue(value, reverse){

						if(!reverse){

							RPSSlider.slider('option', 'value', fromBudgetToRPS(value));
							RPSSliderOutput.text(fromBudgetToRPS(value) / units);
						
						}
						else{

							budgetSlider.slider('option', 'value', fromRPStoBudget(value));
							budgetSliderOutput.text(fromRPStoBudget(value));

						}

					}

					function setMode(mode){

						if(mode !== 1 && mode !== 2) return false;

						if(mode == 1){

							RPSSlider.slider('option', 'disabled', true);
							budgetSlider.slider('option', 'disabled', false);

						}
						else if(mode == 2){

							budgetSlider.slider('option', 'disabled', true);
							RPSSlider.slider('option', 'disabled', false);

						}

					}

					function calculateMarkers(sliderType, min, max, amount, label){
						var distance = (max - min) / amount,
							container = sliderType == 'budget' ? budgetMarkers : RPSMarkers,
							label = label ? ' ' + label : '';
					

						var marker = $('<div></div>', {
							class: 'marker',
							text: ( sliderType == 'RPS' ? (Math.floor(min) / units) + label : Math.floor(min) + label)
						});

						container.append(marker);

						for(var i = 1; i < amount - 1; i++){

							marker = $('<div></div>', {
								class: 'marker',
								text: sliderType == 'RPS' ? (Math.ceil(i * distance) / units) + label  : Math.ceil(i * distance) + label
							});

							container.append(marker);

						}

						marker = $('<div></div>', {
							class: 'marker',
							text: '≥ ' + (sliderType == 'RPS' ? (Math.floor(max) / units) + label : Math.floor(max) + label)
						});

						container.append(marker);

					}

				/*------------------------ End of Logic ---------------------------*/

				/*------------------------ Initialize ---------------------------*/

					if (budgetSlider.length){

						// budget slider init

						budgetSlider.slider($.extend({
								disabled: mode == 1 ? false : true,
								range: "min",
								create: function(event, ui){
									calculateMarkers('budget', budgetSlider.slider('option', 'min'), budgetSlider.slider('option', 'max'), 4);
								},
		 						slide: function( event, ui ) {
		 							calculateValue(ui.value);
							        budgetSliderOutput.text(ui.value);
							    }
							}, budgetSliderSettings)
						);

					    budgetSliderOutput.text(budgetSlider.slider("value"));

					    // RPS slider init

					    RPSSlider.slider(
					    	$.extend({
						    	disabled: mode == 1 ? true : false,
						    	value: fromBudgetToRPS(budgetSliderSettings.value),
						      	range: "min",
						      	create: function(event, ui){
						      		calculateMarkers('RPS', RPSSlider.slider('option', 'min'), RPSSlider.slider('option', 'max'), 5, 'M');
								},
						      	slide: function( event, ui ) {
						      		calculateValue(ui.value, true);
						        	RPSSliderOutput.text(ui.value / units);
						      	}
						    }, RPSSliderSettings)
					    );

					    RPSSliderOutput.text(RPSSlider.slider("value") / units);
					}

					// change mode

					$('body').on('click', '[data-mode]', function(e){

						e.preventDefault();

						var $this = $(this),
							mode = +$this.data('mode'),
							activeElem = $('[data-active-mode-element="'+mode+'"]');

						$this
							.add(activeElem)
							.addClass('active')
							.siblings('[data-mode], [data-active-mode-element]')
							.removeClass('active');

						setMode(mode);

					});

				/*------------------------ End of Initialize ---------------------------*/

			/*------------------------ Start Using ---------------------------*/

				var config = {

					'Centos 6.0' : {
						'Python': ["CentosPython: Pdeb http://tarantool.org/dist/master/debian/ $release main", "deb-src http://tarantool.org/dist/master/debian/ $release main"],
						'PHP': ["CentosPHP: deb http://tarantool.org/dist/master/debian/ $release main", "deb-src http://tarantool.org/dist/master/debian/ $release main"],
						'Ruby': ["CentosRuby: deb http://tarantool.org/dist/master/debian/ $release main", "deb-src http://tarantool.org/dist/master/debian/ $release main"],
						'C++': ["CentosC++: deb http://tarantool.org/dist/master/debian/ $release main", "deb-src http://tarantool.org/dist/master/debian/ $release main"]
					},

					'Linux' : {
						'Python': ["LinuxPython: deb http://tarantool.org/dist/master/debian/ $release main", "deb-src http://tarantool.org/dist/master/debian/ $release main"],
						'PHP': ["LinuxPHP: deb http://tarantool.org/dist/master/debian/ $release main", "deb-src http://tarantool.org/dist/master/debian/ $release main"],
						'Ruby': ["LinuxRuby: deb http://tarantool.org/dist/master/debian/ $release main", "deb-src http://tarantool.org/dist/master/debian/ $release main"],
						'C++': ["LinuxC++: deb http://tarantool.org/dist/master/debian/ $release main", "deb-src http://tarantool.org/dist/master/debian/ $release main"]
					},

					'NetWare 4.x' : {
						'Python': ["deb http://tarantool.org/dist/master/debian/ $release main", "deb-src http://tarantool.org/dist/master/debian/ $release main"],
						'PHP': ["deb http://tarantool.org/dist/master/debian/ $release main", "deb-src http://tarantool.org/dist/master/debian/ $release main"],
						'Ruby': ["deb http://tarantool.org/dist/master/debian/ $release main", "deb-src http://tarantool.org/dist/master/debian/ $release main"],
						'C++': ["deb http://tarantool.org/dist/master/debian/ $release main", "deb-src http://tarantool.org/dist/master/debian/ $release main"]
					},

					'Mac OS X Snow Leopard' : {
						'Python': ["deb http://tarantool.org/dist/master/debian/ $release main", "deb-src http://tarantool.org/dist/master/debian/ $release main"],
						'PHP': ["deb http://tarantool.org/dist/master/debian/ $release main", "deb-src http://tarantool.org/dist/master/debian/ $release main"],
						'Ruby': ["deb http://tarantool.org/dist/master/debian/ $release main", "deb-src http://tarantool.org/dist/master/debian/ $release main"],
						'C++': ["deb http://tarantool.org/dist/master/debian/ $release main", "deb-src http://tarantool.org/dist/master/debian/ $release main"]
					},

					'Unicos' : {
						'Python': ["deb http://tarantool.org/dist/master/debian/ $release main", "deb-src http://tarantool.org/dist/master/debian/ $release main"],
						'PHP': ["deb http://tarantool.org/dist/master/debian/ $release main", "deb-src http://tarantool.org/dist/master/debian/ $release main"],
						'Ruby': ["deb http://tarantool.org/dist/master/debian/ $release main", "deb-src http://tarantool.org/dist/master/debian/ $release main"],
						'C++': ["deb http://tarantool.org/dist/master/debian/ $release main", "deb-src http://tarantool.org/dist/master/debian/ $release main"]
					},

					'NeXTStep' : {
						'Python': ["deb http://tarantool.org/dist/master/debian/ $release main", "deb-src http://tarantool.org/dist/master/debian/ $release main"],
						'PHP': ["deb http://tarantool.org/dist/master/debian/ $release main", "deb-src http://tarantool.org/dist/master/debian/ $release main"],
						'Ruby': ["deb http://tarantool.org/dist/master/debian/ $release main", "deb-src http://tarantool.org/dist/master/debian/ $release main"],
						'C++': ["deb http://tarantool.org/dist/master/debian/ $release main", "deb-src http://tarantool.org/dist/master/debian/ $release main"]
					},

					'Rhapsody' : {
						'Python': ["deb http://tarantool.org/dist/master/debian/ $release main", "deb-src http://tarantool.org/dist/master/debian/ $release main"],
						'PHP': ["deb http://tarantool.org/dist/master/debian/ $release main", "deb-src http://tarantool.org/dist/master/debian/ $release main"],
						'Ruby': ["deb http://tarantool.org/dist/master/debian/ $release main", "deb-src http://tarantool.org/dist/master/debian/ $release main"],
						'C++': ["deb http://tarantool.org/dist/master/debian/ $release main", "deb-src http://tarantool.org/dist/master/debian/ $release main"]
					}

				},

				osSelect = $('.os-select'),
				langSelect = $('.language-select'),
				outPut = $('.start_using_text_block');



				osSelect.add(langSelect).on('change', function(){

					var $this = $(this),
						os = osSelect[1].value,
						lang = langSelect[1].value;

					outPut.html(config[os][lang].join("<br>"));

				});

		});