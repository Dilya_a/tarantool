

//  /*================================================>  
//                                 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  INCLUDE AND INITIALIZE Plugins START  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//  <================================================*/
var highlite_menu = function(){
    $('article').each(function(){
        var highlight = function($el){
            var id = $el.attr("id"),
                $menulia = $('.list_plagination>li');
            $('.list_plagination>li').removeClass('active');
            $('.list_plagination').find("a[href="+"#"+id+"]").parent().addClass('active');
        }
        if ($(this)[0].id == 'contacts_page' && $(window).height() > 815){
            if ($(window).scrollTop() >= ( $(this).offset().top -  360 ) && $(window).scrollTop() < ( $(this).offset().top + 560 )    ){
                highlight($(this));
                // console.log($(this))
            }

        } else {
            if ($(window).scrollTop() >= ( $(this).offset().top - 160 ) && $(window).scrollTop() < ( $(this).offset().top + 260 )    ){
                highlight($(this));
            }
        }

    })

}



		var sticky = $(".sticky"),
			tabs = $('.tabs'),
			matchheight = $("[data-mh]"),
		    styler = $(".styler"),
		    owl = $(".owl-carousel"),
		    flex = $(".flexslider"),
		    royalslider = $(".royalslider"),
			wow = $(".wow"),
			popup = $("[data-popup]"),
			superfish = $(".superfish"),
			accordion = $(".accordion")
			windowW = $(window).width(),
			windowH = $(window).height(),
			rangeSlider = $(".r_slider");


			if(sticky.length){
					include("plugins/sticky.js");
					include("plugins/jquery.smoothscroll.js");
			}
			if(rangeSlider.length){
				include("plugins/range_slider/jquery-ui.min.js");
			}
			if(matchheight.length){
				include("plugins/jquery.matchHeight-min.js");
			}
			if(styler.length){
					include("plugins/formstyler/formstyler.js");
			}
			if(flex.length){
					include('plugins/flexslider/jquery.flexslider.js');
			}
			if(owl.length){
					include('plugins/owl-carousel/owl.carousel.js');
			}

			if(accordion.length){
					include('plugins/jquery-ui.js');
			}


			include('js/calculator.js');




			function include(url){ 

					document.write('<script src="'+ url + '"></script>'); 

			}

		


		$(document).ready(function(){




			/* ------------------------------------------------
			STICKY START
			------------------------------------------------ */

					if (sticky.length){
						$(sticky).sticky({
					        topspacing: 0,
					        styler: 'is-sticky',
					        animduration: 0,
					        unlockwidth: false,
					        screenlimit: false,
					        sticktype: 'alonemenu'
						});
					};

			/* ------------------------------------------------
			STICKY END
			------------------------------------------------ */




			/* ------------------------------------------------
			FORMSTYLER START
			------------------------------------------------ */

					if (styler.length){
						styler.styler({
							// selectSmartPositioning: true
						});
					}

			/* ------------------------------------------------
			FORMSTYLER END
			------------------------------------------------ */




			/* ------------------------------------------------
			FLEXSLIDER START
			------------------------------------------------ */

					if(flex.length){
						flex.flexslider({
						    animation: "slide",
						    controlNav: true,
							animationLoop: false,
							slideshow: false
						});
					}

			/* ------------------------------------------------
			FLEXSLIDER END
			------------------------------------------------ */


			/* ------------------------------------------------
			OWL START
			------------------------------------------------ */

					if(owl.length){
						owl.owlCarousel({
							singleItem : true,
							items : 1,
							// loop: true,
							smartSpeed:1000,
							nav: true
							// autoHeight:true
						});
					}

					

			/* ------------------------------------------------
			OWL END
			------------------------------------------------ */




			/* ------------------------------------------------
			TABS START
			------------------------------------------------ */

					if(tabs.length){
						tabs.easyResponsiveTabs();
					}

			/* ------------------------------------------------
			TABS END
			------------------------------------------------ */


			/* ------------------------------------------------
			MATCHHEIGHT START
			------------------------------------------------ */

					if(matchheight.length){
						matchheight.matchHeight();
					}

			/* ------------------------------------------------
			MATCHHEIGHT END
			------------------------------------------------ */


			$(".menu_item, .header_carousel_btn, .list_plagination_item").on('click',function(event){
			    var currentItem = $(this),
			        anchor = currentItem.find(">a").attr("href");

			    
			    $(currentItem).parent().find("li").removeClass("active"); 
			    $(currentItem).addClass("active"); 
			    $('html, body').stop().animate({
			        scrollTop: $("#content, #header").find(anchor).offset().top 
			    }, 1200,'easeOutCubic', function(){
			    	highlite_menu();
			    });
			    event.preventDefault();
			  });

		});

$(window).on("scroll", function(){
	highlite_menu();
})




//  /*================================================>  
//                                 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  INCLUDE AND INITIALIZE Plugins END    <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//  <================================================
